﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkSpawner : MonoBehaviour {

	public float layerHeight = 50.0f;
	private bool reachTop = false;
	
	public float initialSeparation = 0.5f; // An initial percentage of the separation between platforms

	public List<GameObject> chunkPrefabs;
	public GameObject winChunkPrefab;
	
	public GameObject tileTop;
	public GameObject tileCentre;
	public GameObject tileBottom;
	private float tileHeight = 10.08f;
	
	public Camera cam;
	
	private float currentChunkHeight = 0.0f;
	private float currentBackgroundHeight = 0.0f;

	// Use this for initialization
	void Start () {
		
		SpawnChunks();
		
		SpawnBackground();

	}
	
	public void SpawnChunks() {
		List<GameObject> chunksLeft = new List<GameObject>(chunkPrefabs);
		List<bool> mirroredStates = new List<bool>();
		for(int i = 0; i < chunksLeft.Count; i++) {
			mirroredStates.Add((Random.Range(0, 2) == 1));
		}
		List<bool> mStatesLeft = new List<bool>(mirroredStates);
		
		int poolNo = 1;
		
		while(reachTop == false) {
			
			int chunkNo = Random.Range(0, chunksLeft.Count);
			
			// Spawn randomly selected chunk at currentChunkHeight with its mirrored state
			GameObject currentChunk = Instantiate(chunksLeft[chunkNo], new Vector2(0.0f, currentChunkHeight), Quaternion.identity, transform);
			currentChunk.name = "P"+poolNo+" "+currentChunk.name;
			if(mStatesLeft[chunkNo] == true) {
				foreach(Transform platform in currentChunk.transform) {
					platform.localPosition = new Vector2(-platform.localPosition.x, platform.localPosition.y);
				}
				currentChunk.name = currentChunk.name+" M";
			}
			// Remove chunk from pool
			chunksLeft.RemoveAt(chunkNo);
			mStatesLeft.RemoveAt(chunkNo);
			
			
			foreach(Transform platform in currentChunk.transform) {
				float originalHeightPercentage = (float)platform.position.y/layerHeight;
				if (originalHeightPercentage > 1.0f) {
					Destroy(platform.gameObject);
					reachTop = true;
				} else {
					// Scale all objects' heights
					float percentageBetweenRange = (initialSeparation + (float)originalHeightPercentage * (1.0f - initialSeparation)); // starts at initial separation, goes up to 100%
					float newHeight = (float)platform.localPosition.y * percentageBetweenRange;
					platform.localPosition = new Vector2(platform.localPosition.x, newHeight);
					
					// Scale vertical moving platforms' radii
					if(platform.gameObject.GetComponent<MoveObject>() == true) { // Moving platform vertical
						MoveObject moveScript = platform.gameObject.GetComponent<MoveObject>();
						if(moveScript.vertical == true) {
							float newRadius = (float)moveScript.radius * percentageBetweenRange;
							moveScript.radius = newRadius;
						}
					}
					
					// Find and set height of current chunk
					if(platform.position.y > currentChunkHeight) {
						currentChunkHeight = platform.position.y;
						// For moving platforms, add half their radius to this value
					}
				}
			}
			
			
			if(chunksLeft.Count <= 0) {
				// Refill the pool
				chunksLeft = new List<GameObject>(chunkPrefabs);
				poolNo++;
				// Flip mirrored states
				for(int i = 0; i < mirroredStates.Count; i++) {
					mirroredStates[i] = !mirroredStates[i];
				}
				mStatesLeft = new List<bool>(mirroredStates);
			}
			
		}
		
		// Then scale
		/*
		foreach(Transform chunk in gameObject.transform) {
			float originalHeightPercentageC = (float)chunk.position.y/layerHeight;

			float percentageBetweenRangeC =  (initialSeparation + (float)originalHeightPercentageC * (1.0f - initialSeparation)); // starts at initial separation, goes up to 100%
			float newHeightC = (float)chunk.position.y * percentageBetweenRangeC;
			chunk.position = new Vector2(chunk.position.x, newHeightC);
			foreach(Transform platform in chunk.gameObject.transform) {
				float originalHeightPercentageP = (float)platform.position.y/layerHeight;
				float percentageBetweenRangeP =  (initialSeparation + (float)originalHeightPercentageP * (1.0f - initialSeparation)); // starts at initial separation, goes up to 100%
				Debug.Log(chunk.name + " " + platform.name + ": " + originalHeightPercentageP);
				float newHeightP = (float)platform.localPosition.y * percentageBetweenRangeP;
				platform.localPosition = new Vector2(platform.localPosition.x, newHeightP);
			}
		}
		*/
		
		// Spawn win zone at true height
		//GameObject vinChunk = Instantiate(winChunkPrefab, new Vector2(0.0f, layerHeight), Quaternion.identity, transform);
		// Spawn win zone
		GameObject winChunk = Instantiate(winChunkPrefab, new Vector2(0.0f, currentChunkHeight), Quaternion.identity, transform);
	}
	
	public void SpawnBackground() {
		
		for(currentBackgroundHeight = 0.0f; currentBackgroundHeight <= currentChunkHeight + tileHeight*2; currentBackgroundHeight += tileHeight) {
			if(currentBackgroundHeight == 0.0f) {
				GameObject currentTile = Instantiate(tileBottom, new Vector2(0.0f, currentBackgroundHeight), Quaternion.identity, transform);
			} else if(currentBackgroundHeight >= currentChunkHeight + tileHeight) {
				GameObject currentTile = Instantiate(tileTop, new Vector2(0.0f, currentBackgroundHeight), Quaternion.identity, transform);
			} else {
				GameObject currentTile = Instantiate(tileCentre, new Vector2(0.0f, currentBackgroundHeight), Quaternion.identity, transform);
			}
		}
		
	}
	
	public void TurnOffLowerPlatforms() {
		
		float destroyLine = cam.ScreenToWorldPoint (new Vector2 (0, -33)).y;
		
		foreach(Transform chunk in gameObject.transform) {
			foreach(Transform platform in chunk.gameObject.transform) {
				if(platform.gameObject.GetComponent<BoxCollider2D>() != null) {
					BoxCollider2D currentCollider = platform.gameObject.GetComponent<BoxCollider2D>();
					if(platform.gameObject.transform.position.y < destroyLine) {
						currentCollider.enabled = false;
					} else {
						currentCollider.enabled = true;
					}
				}
			}
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		TurnOffLowerPlatforms();
	}
}
