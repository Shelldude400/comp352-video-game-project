﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MoveCamera : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;
	public GameObject loseMenu;
	public GameObject winMenu;
	public Text collectableText;
	private Camera cam;
	private float timePassed;
	
	// Use this for initialization
	void Start () {
		cam = GetComponent<Camera> ();
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 highPos = new Vector3(this.transform.position.x, Mathf.Min (player1.transform.position.y, player2.transform.position.y), this.transform.position.z);
		if (this.transform.position.y < highPos.y) {
			this.transform.position = highPos;
		}

		float bottomCam = cam.ScreenToWorldPoint (new Vector2 (0f, 0f)).y;


		if((player1.transform.position.y < bottomCam) && (player2.transform.position.y < bottomCam)) {
			if (loseMenu) {
				loseMenu.SetActive (true);
				player1.GetComponent<PlayerController> ().dead = true;
				player2.GetComponent<PlayerController> ().dead = true;
			}
		}

		if (player1.GetComponent<PlayerController>().inWinZone && player2.GetComponent<PlayerController>().inWinZone) {
			if (winMenu) {
				winMenu.SetActive (true);
				Time.timeScale = 0;
			}
		}

		UpdateCollectables ();
	}


	void UpdateCollectables() {
		if (collectableText) {
			int total = player1.GetComponent<PlayerController> ().collectCount + player2.GetComponent<PlayerController> ().collectCount;
			collectableText.text = "x" + total.ToString ();
		}
	}
}
