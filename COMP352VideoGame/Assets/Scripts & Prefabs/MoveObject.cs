﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour {

	public float moveSpeed = 1f;
	public float radius = 5f;
	[HideInInspector] public float moveCount = 0f;
	public bool vertical = false;
	private Rigidbody2D rb;
	private Transform transform;

	// Use this for initialization
	void Start () {
		transform = GetComponent<Transform> ();
		rb = GetComponent<Rigidbody2D> ();
	}

	void Awake() {
		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (vertical) {
			rb.MovePosition (new Vector3 (transform.position.x, transform.position.y + moveSpeed * Time.deltaTime));
		} else {
			rb.MovePosition (new Vector3 (transform.position.x + moveSpeed * Time.deltaTime, transform.position.y));
		}
		moveCount += moveSpeed * Time.deltaTime;
		if (moveCount <= -radius || moveCount >= radius) {
			moveSpeed = -moveSpeed;
		}
	}
}
