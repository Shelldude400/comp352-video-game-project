﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour {

	public Toggle simpleCon;
	public PlayerController player1;
	public PlayerController player2;
	public Slider volumeSlider;
	public AudioClip clickSound;
	public GameObject simpleConMenu;
	public GameObject conMenu;


	// Use this for initialization
	void Awake () {
		if (PlayerPrefs.HasKey ("SimpleControls")) {
			int x = PlayerPrefs.GetInt ("SimpleControls");
			toggleControls (x != 0); // if simple controls have not been set default to off
		} else {
			toggleControls (false);
		}
	
	 if (PlayerPrefs.HasKey("AudioVolume")) {
            AudioListener.volume = PlayerPrefs.GetFloat("AudioVolume");
			volumeSlider.value = PlayerPrefs.GetFloat("AudioVolume");
        } else {
            // first time the game is run, use the default value for audio slider
			AudioListener.volume = 0.5f;
			volumeSlider.value = 0.5f;
        }

		Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update () {
		AudioListener.volume = volumeSlider.value;
        PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);
	}

	public void toggleControls(bool value) {
		if (player1) {
			player1.altCon = !value;
		}
		if (player2) {
			player2.altCon = !value;
		}
		if (value) {
			PlayerPrefs.SetInt ("SimpleControls", 1);
			simpleCon.isOn = true;
		} else {
			PlayerPrefs.SetInt ("SimpleControls", 0);
			simpleCon.isOn = false;
		}
		
	}

	public void playClick() {
		SoundManager.instance.PlaySingle (clickSound);
	}

	public void ShowControls() {
		if (simpleCon.isOn) {
			simpleConMenu.SetActive (true);
		} else {
			conMenu.SetActive (true);
		}
	}
}
