using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausedGame : MonoBehaviour {
	public bool Paused = false;
	public GameObject pauseMenu; 
	public GameObject optionsMenu;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Escape)) {
		
			togglePaused();
		}

	}
	
	public void togglePaused() {
		
		
		if (Paused) {
			Time.timeScale = 1;
			pauseMenu.SetActive(false);
			optionsMenu.SetActive (false);
		} else {
			Time.timeScale = 0;
			pauseMenu.SetActive(true);
		}
		Paused = !Paused;
	}

}
		


