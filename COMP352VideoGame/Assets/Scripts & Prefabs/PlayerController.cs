﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	[HideInInspector] public bool jump = false;
	public int playerNo;
	public float moveForce = 80f;
	public float maxSpeed = 5f;
	public float jumpForce = 12000f;
	public float groundSpeed = 1f;
	public float upwardAssistForce = 100;
	public bool anchored = false;
	public bool grounded = false;
	bool zeroed = false;
	private Rigidbody2D rigidbody; 
	public bool inWinZone = false;
	public bool inCrystalZone = false;
	public Rope ropeScript;
	public float ropeDelay = 10.0f;
	private float timeLeft;
	private SpriteRenderer sprite;
	public bool altCon = false;
	private Animator anim;
	private float platformSpeed = 0f;
	private float platformCount = 0f;
	private float platformRadius = 0f;
	private bool platformVertical = false;
	public int collectCount = 0;
	public AudioClip jumpSound;
	public AudioClip anchorSound;
	bool playAnchor;
	public AudioClip unanchorSound;
	bool playUnanchor;
	public AudioSource swingSound;
	public float swingVolSensitivity = 40f;
	public float swingPitSensitivity = 10f;
	public bool triggered = false;
	
	public bool dead = false;
	
	// Variables for clamping players' y position:
	private bool isLocked = false;
	private float lockedY;

	// Use this for initialization
	void Awake () {
		rigidbody = GetComponent<Rigidbody2D> ();
		sprite = GetComponent<SpriteRenderer> ();
		anim = GetComponent<Animator> ();
		timeLeft = Time.fixedTime + ropeDelay;
	}
	 
	void Update() {
		// Ray casts and checks if you're really close to the ground. Also checks vertical movement direction
		//grounded = (Physics2D.Linecast (transform.position, groundCheckL.position, 1 << LayerMask.NameToLayer ("Ground")) && (rigidbody.velocity.y <= 0.02f || platformVertical && rigidbody.velocity.y <= 0.02f + platformSpeed));
		//grounded = grounded || (Physics2D.Linecast (transform.position, groundCheckR.position, 1 << LayerMask.NameToLayer ("Ground")) && (rigidbody.velocity.y <= 0.02f || platformVertical && rigidbody.velocity.y <= 0.02f + platformSpeed));

		if(!anchored) {
			if(altCon && playerNo == 1) {
				if ((Input.GetAxisRaw ("Vertical") < 0) && (grounded || inCrystalZone)) {
					anchored = true;
					zeroed = false;
					playAnchor = true;
					playUnanchor = true;
				}
			} else if ((Input.GetAxisRaw ("Vertical" + playerNo) < 0) && (grounded || inCrystalZone)) {
				anchored = true;
				zeroed = false;
				playAnchor = true;
				playUnanchor = true;
			}
		}

		if(!jump) {
			if (altCon && playerNo == 1) {
				if ((Input.GetAxisRaw ("Vertical") > 0) && grounded && !anchored) {
					jump = true;
				}
			} else if ((Input.GetAxisRaw ("Vertical" + playerNo) > 0) && grounded && !anchored) {
				jump = true;
			}
		}

		if(!grounded) {
			if(anchored && !inCrystalZone) {
				anchored = false;
			}

			// Play swing sound
			if(dead) {
				if (swingSound.volume > 0) { // Fade out
					swingSound.volume -= 0.2f * Time.deltaTime;
					swingSound.pitch -= 0.2f * Time.deltaTime;
				} else {
					swingSound.Stop ();
				}
			} else {
				swingSound.volume = rigidbody.velocity.magnitude/swingVolSensitivity;
				swingSound.pitch = rigidbody.velocity.magnitude/swingPitSensitivity;
			}
		} else {
			swingSound.volume = 0.0f;
		}
		
		/* A way to clamp players' y position to stop sprite glitching:
		if(grounded) {
			if(isLocked) {
				transform.position = new Vector2(transform.position.x, lockedY);
			} else {
				isLocked = true;
				lockedY = transform.position.y;
			}
			if(rigidbody.velocity.y > 0.02f) {
				isLocked = false;
			}
		} else {
			isLocked = false;
		}
		*/



	}
	// Update is called once per frame
	void FixedUpdate () {

		float x;
		float z;


		if(rigidbody.velocity.y > 0.02f) {
			rigidbody.AddForce (Vector2.up * upwardAssistForce);
		}

		if (altCon) {

			if (playerNo == 1) {
				x = Input.GetAxis ("Horizontal");
				z = Input.GetAxisRaw ("Horizontal");
			} else {
				x = Input.GetAxis ("Horizontal" + playerNo);
				z = Input.GetAxisRaw ("Horizontal" + playerNo);
			}

			if(grounded) {


				if (!anchored) {
					if (platformVertical) {

						rigidbody.velocity = new Vector2 (x * groundSpeed, platformSpeed);
						if (Mathf.Abs (rigidbody.velocity.x) > maxSpeed) {
							rigidbody.velocity = new Vector2 (Mathf.Sign (rigidbody.velocity.x) * maxSpeed, rigidbody.velocity.y);
						}
					} else {
						rigidbody.velocity = new Vector2 (x * groundSpeed + platformSpeed, 0);
						if (Mathf.Abs (rigidbody.velocity.x) > maxSpeed + platformSpeed) {
							rigidbody.velocity = new Vector2 (Mathf.Sign (rigidbody.velocity.x) * maxSpeed + platformSpeed, rigidbody.velocity.y);
						}
					}
					if (z < 0) {
						changeState (2);
					} else if (z > 0) {
						changeState (1);
					} else {
						changeState (0);
					}
				}
			} else {
				rigidbody.AddForce (Vector2.right * x * moveForce);

				if (z < 0.0f) {
					changeState (6);
				} else if (z > 0.0f) {
					changeState (7);
				} else {
					changeState (5);
				}
				
			}
			
			if (grounded || anchored) {
				if (Input.GetAxisRaw ("RopeCon1") == 0 && Input.GetAxisRaw ("RopeCon2") == 0) {
					ropeScript.pleasePlay[playerNo] = false;
				} else if (Input.GetAxisRaw ("RopeCon" + playerNo) < 0) {
					if (Time.fixedTime >= timeLeft) {
						ropeScript.RetractRope (playerNo);
						ropeScript.pleasePlay[playerNo] = true;
						timeLeft = Time.fixedTime + ropeDelay;
					}
				} else if (Input.GetAxisRaw ("RopeCon" + playerNo) > 0) {
					if (Time.fixedTime >= timeLeft) {
						ropeScript.ExtendRope (playerNo);
						ropeScript.pleasePlay[playerNo] = true;
						timeLeft = Time.fixedTime + ropeDelay;
					}
				}
			} else {
				ropeScript.pleasePlay[playerNo] = false;
			}


			if (anchored) {
				if (inCrystalZone) {
					changeState (4);
				} else {
					changeState (3);
				}
				rigidbody.velocity = new Vector2 (0, 0);
				rigidbody.isKinematic = true;
				if (platformVertical) {
					rigidbody.MovePosition (new Vector3 (transform.position.x, transform.position.y + platformSpeed * Time.deltaTime));
				} else {
					rigidbody.MovePosition (new Vector3 (transform.position.x + platformSpeed * Time.deltaTime, transform.position.y));
				}
				if (playerNo == 1) {
					if (Input.GetAxisRaw ("Vertical") >= 0) {
						anchored = false;
					}
				} else {
					if (Input.GetAxisRaw ("Vertical" + playerNo) >= 0) {
						anchored = false;
					}
				}
				if (playAnchor) {
					playAnchor = false;
					SoundManager.instance.RandomiseSFX(anchorSound);
				}
			} else {
				rigidbody.isKinematic = false;
				if (playUnanchor) {
					playUnanchor = false;
					SoundManager.instance.RandomiseSFX(unanchorSound);
				}
			}


		} else {

			x = Input.GetAxis ("Horizontal"+playerNo);
			z = Input.GetAxisRaw ("Horizontal" + playerNo);

			if(grounded) {
				if (platformVertical) {

					rigidbody.velocity = new Vector2 (x * groundSpeed, platformSpeed);
					if (Mathf.Abs (rigidbody.velocity.x) > maxSpeed) {
						rigidbody.velocity = new Vector2 (Mathf.Sign (rigidbody.velocity.x) * maxSpeed, rigidbody.velocity.y);
					}
				} else {
					rigidbody.velocity = new Vector2 (x * groundSpeed + platformSpeed, 0);
					if (Mathf.Abs (rigidbody.velocity.x) > maxSpeed + platformSpeed) {
						rigidbody.velocity = new Vector2 (Mathf.Sign (rigidbody.velocity.x) * maxSpeed + platformSpeed, rigidbody.velocity.y);
					}
				}
				if (!anchored) {
					if (z < 0) {
						changeState (2);
					} else if (z > 0) {
						changeState (1);
					} else {
						changeState (0);
					}
				}
			}
			else {
				rigidbody.AddForce (Vector2.right * x * moveForce);
				if (z < 0.0f) {
					changeState (6);
				} else if (z > 0.0f) {
					changeState (7);
				} else {
					changeState (5);
				}

			}

			if (anchored) {
				if (inCrystalZone) {
					changeState (4);
				} else {
					changeState (3);
				}
				rigidbody.velocity = new Vector2 (0, 0);
				rigidbody.isKinematic = true;
				if (platformVertical) {
					rigidbody.MovePosition (new Vector3 (transform.position.x, transform.position.y + platformSpeed * Time.deltaTime));
				} else {
					rigidbody.MovePosition (new Vector3 (transform.position.x + platformSpeed * Time.deltaTime, transform.position.y));
				}
				if (Input.GetAxisRaw ("Vertical" + playerNo) == 0) {
					ropeScript.pleasePlay[playerNo] = false;
				} else if (Input.GetAxisRaw ("Vertical" + playerNo) < 0) {
					if (Time.fixedTime >= timeLeft) {
						ropeScript.ExtendRope (playerNo);
						ropeScript.pleasePlay[playerNo] = true;
						timeLeft = Time.fixedTime + ropeDelay;
					}
				} else if (Input.GetAxisRaw ("Vertical" + playerNo) > 0) {
					if (Time.fixedTime >= timeLeft) {
						ropeScript.RetractRope (playerNo);
						ropeScript.pleasePlay[playerNo] = true;
						timeLeft = Time.fixedTime + ropeDelay;
					}
				}
				if (z == 0) {
					zeroed = true;
					//Input.ResetInputAxes(); // Make sure the player starts 'still' (affects ALL players...)
				} else if (zeroed) {

					anchored = false;
					rigidbody.isKinematic = false;
				}

				if (playAnchor) {
					playAnchor = false;
					SoundManager.instance.RandomiseSFX(anchorSound);
				}

			} else {
				rigidbody.isKinematic = false;
				if (playUnanchor) {
					playUnanchor = false;
					SoundManager.instance.RandomiseSFX(unanchorSound);
				}
				
				ropeScript.pleasePlay[playerNo] = false;
			}
		}




		if (jump) {
			rigidbody.AddForce (new Vector2 (0f, jumpForce));
			jump = false;

			SoundManager.instance.PlaySingle(jumpSound);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "WinZone") {
			inWinZone = true;
		}

		if (other.gameObject.tag == "Collectable") {
			if (!triggered) {
				collectCount++;
				AudioSource gemSound = other.GetComponent<AudioSource> ();
				SoundManager.instance.PlaySingle (gemSound.clip);
				triggered = true;
			} else {
				Destroy (other.gameObject);
				triggered = false;
			}
		}

		if (other.gameObject.tag == "CrystalZone") {
			inCrystalZone = true;
		}

		if (other.gameObject.tag == "Moving") {

			platformSpeed = other.gameObject.GetComponent<MoveObject> ().moveSpeed;
			platformCount = other.gameObject.GetComponent<MoveObject> ().moveCount;
			platformRadius = other.gameObject.GetComponent<MoveObject> ().radius;
			platformVertical = other.gameObject.GetComponent<MoveObject> ().vertical;


		}

		if (other.gameObject.layer == 8 && rigidbody.velocity.y <= 0.02f || platformVertical && rigidbody.velocity.y <= 0.02f + platformSpeed) {
			grounded = true;
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.tag == "WinZone") {
			inWinZone = false;
		}
		if (other.gameObject.tag == "CrystalZone") {
			inCrystalZone = false;
		}
		
		if (other.gameObject.tag == "Moving") {

			platformSpeed = 0f;
			platformCount = 0f;
			platformRadius = 0f;

			platformVertical = false;

		}
		if (other.gameObject.layer == 8) {
			grounded = false;
		}
	}


	void changeState(int state) {
		anim.SetInteger ("state", state);
	}

	void OnTriggerStay2D(Collider2D other) {



		if (other.gameObject.tag == "Moving") {

			platformCount += platformSpeed * Time.deltaTime;
			if (platformCount <= -platformRadius || platformCount >= platformRadius) {
				platformSpeed = -platformSpeed;
			}
			if (other.gameObject.layer == 8 && rigidbody.velocity.y <= 0.02f || platformVertical && rigidbody.velocity.y <= 0.02f + platformSpeed) {
				grounded = true;
			}

		}

	}
}
