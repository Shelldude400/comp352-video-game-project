﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionDropdown : MonoBehaviour {
public Dropdown dropDownItem;
public Text selectedText;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void Dropdown_Index_Changed(int index)
	{
		Debug.Log("oh no"+ index);
		switch(index)
		{
			case 0:
			Screen.SetResolution(640,480,false);
			break;
			
			case 1:
			Screen.SetResolution(800,600,false);
			break;
			
			case 2:
			Screen.SetResolution(1024,768,false);
			break;
			
			case 3:
			Screen.SetResolution(1280,720,false);
			break;
			
			case 4:
			Screen.SetResolution(1600,900,false);
			break;
			
			case 5:
			Screen.SetResolution(1920,1020,false);
			break;
		}
			
	}
}
