﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour {

	public GameObject braidPrefab;
	public int initialNumBraids = 7;
	public int minBraids = 2;
	public int maxBraids = 20;
	public GameObject connectedPlayer;
	public float playerDistanceFromRope = 0.3f;
	
	public List<GameObject> listOfBraids = new List<GameObject>();
	
	private LineRenderer lr;
	
	private int startNo;
	private int endNo;
	
	public AudioClip tautSound;
	bool playTaut;
	public float ropeTwangForce = 300.0f;
	public float twangDelay = 0.5f;
	private float timeLeft;
	
	public AudioSource feedSound;
	public bool playFeed = false;
	public bool[] pleasePlay = new bool[]{ false, false, false };

	// Use this for initialization
	void Start () {
		
		lr = GetComponent<LineRenderer>();
		
		GenerateRope();
		
		timeLeft = Time.fixedTime + twangDelay;
	}
	
	public void GenerateRope() {
		
		// Add self to front of list
		listOfBraids.Add(transform.gameObject);
		
		Rigidbody2D prevRB = gameObject.GetComponent<Rigidbody2D>();
		
		for(int i = 0; i < initialNumBraids; i++) {
			GameObject newBraid = Instantiate(braidPrefab, transform);
			newBraid.name = "Braid"+listOfBraids.Count;
			HingeJoint2D joint = newBraid.GetComponent<HingeJoint2D>();
			joint.autoConfigureConnectedAnchor = false;
			joint.connectedBody = prevRB;
			
			if(i < initialNumBraids - 1) {
				prevRB = newBraid.GetComponent<Rigidbody2D>();
				listOfBraids.Add(newBraid);
			} else {
				ConnectRopeEnd(newBraid.GetComponent<Rigidbody2D>());
				listOfBraids.Add(newBraid);
			}
		}

		startNo = 0;
		endNo = listOfBraids.Count;
	}
	
	void ConnectRopeEnd(Rigidbody2D endRB) {
		
		HingeJoint2D playerJoint = connectedPlayer.GetComponent<HingeJoint2D>();
		
		if(playerJoint == null) {
			playerJoint = connectedPlayer.AddComponent<HingeJoint2D>();
			playerJoint.autoConfigureConnectedAnchor = false;
		}
		
		playerJoint.connectedBody = endRB;
		playerJoint.anchor = Vector2.zero;
		playerJoint.connectedAnchor = new Vector2 (0f, -playerDistanceFromRope);
	}
	
	void RenderLine() {
		
		lr.SetVertexCount(listOfBraids.Count+2);
		
		GameObject holdingPlayer = gameObject.transform.parent.gameObject;
		lr.SetPosition(0, holdingPlayer.transform.position);
		for(int i = 0; i < listOfBraids.Count; i++) {
			lr.SetPosition(i+1, listOfBraids[i].transform.position);
		}
		lr.SetPosition(listOfBraids.Count+1, connectedPlayer.transform.position);
		
		if (listOfBraids.Count <= maxBraids) {
			lr.material.color = new Color(1f, 1f, 1f, 1f);
			playTaut = true;
			if (listOfBraids.Count >= maxBraids/2) { 
				float redness = ((float)listOfBraids.Count/(float)maxBraids) - .5f;
				lr.material.color = new Color(1f, 1f - redness, 1f - redness, 1f);
			}
		} else {
			if(playTaut) {
				// Make rope twang
				if (Time.fixedTime >= timeLeft) {
					// Sound
					playTaut = false;
					SoundManager.instance.RandomiseSFX(tautSound);
					// Tug motion
					for(int i = 1; i < listOfBraids.Count; i++) { // Offsets of 1 account for holdingPlayer not being moved
						GameObject currentBraid = listOfBraids [i];
						Rigidbody2D braidRB = currentBraid.GetComponent<Rigidbody2D>();
						braidRB.AddForce (Vector2.up * ropeTwangForce);
					}
					timeLeft = Time.fixedTime + twangDelay;
				}
			}
			lr.material.color = new Color(0.9f, 0.2f, 0.2f, 1f);
		}
		
	}

	public void ExtendRope(int playerNo) { // Add a braid to the end of the rope and connect it to the other player
		if (listOfBraids.Count <= maxBraids) {
			if(playerNo == 1) { // Add to start
				
				GameObject newBraid = Instantiate (braidPrefab, transform);
				newBraid.name = "Braid" + startNo;
				HingeJoint2D newBraidJoint = newBraid.GetComponent<HingeJoint2D> ();
				newBraidJoint.autoConfigureConnectedAnchor = false;
			
				GameObject firstBraid = listOfBraids [1]; // 0 is Rope Connector
				HingeJoint2D firstBraidJoint = firstBraid.GetComponent<HingeJoint2D> ();
				Rigidbody2D baseRB = gameObject.GetComponent<Rigidbody2D>();
				Rigidbody2D newBraidRB = newBraid.GetComponent<Rigidbody2D>();
			
				newBraidJoint.connectedBody = baseRB;
				firstBraidJoint.connectedBody = newBraidRB;
			
				listOfBraids.Insert (1, newBraid);
				
				startNo--;
			}
			if(playerNo == 2) { // Add to end
				GameObject newBraid = Instantiate (braidPrefab, transform);
				newBraid.name = "Braid" + endNo;
				HingeJoint2D newBraidJoint = newBraid.GetComponent<HingeJoint2D> ();
				newBraidJoint.autoConfigureConnectedAnchor = false;
			
				GameObject lastBraid = listOfBraids [listOfBraids.Count - 1];
				Rigidbody2D lastRB = lastBraid.GetComponent<Rigidbody2D> ();
			
				newBraidJoint.connectedBody = lastRB;

				ConnectRopeEnd (newBraid.GetComponent<Rigidbody2D> ());
			
				listOfBraids.Insert (listOfBraids.Count, newBraid);
				
				endNo++;
			}
			
		}
	}
	
	public void RetractRope(int playerNo) {
		// Connect and Destroy
		if (listOfBraids.Count >= minBraids) {
			if(playerNo == 1) { // Remove from start
				GameObject firstBraid = listOfBraids [1]; // 0 is Rope Connector
				GameObject secondBraid = listOfBraids [2];
				Rigidbody2D baseRB = gameObject.GetComponent<Rigidbody2D>();
				HingeJoint2D joint = secondBraid.GetComponent<HingeJoint2D>();
				joint.connectedBody = baseRB;
			
				listOfBraids.Remove (firstBraid);
				Destroy (firstBraid);
				
				startNo++;
			}
			if(playerNo == 2) { // Remove from end
				GameObject lastBraid = listOfBraids [listOfBraids.Count - 1];
				GameObject secondLastBraid = listOfBraids [listOfBraids.Count - 2];
			
				ConnectRopeEnd (secondLastBraid.GetComponent<Rigidbody2D> ());
			
				listOfBraids.Remove (lastBraid);
				Destroy (lastBraid);
				
				endNo--;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		RenderLine();
		
		if(listOfBraids.Count < minBraids || listOfBraids.Count > maxBraids || (pleasePlay[1] == false && pleasePlay[2] == false)) {
			playFeed = false;
		} else {
			playFeed = true;
		}
		
		if(playFeed == true) {
			float lengthPercentage = (float)listOfBraids.Count/(float)maxBraids;
			feedSound.pitch = 1.5f - lengthPercentage;
			feedSound.volume = 0.25f + lengthPercentage/5;
		} else {
			feedSound.volume = 0.0f;
		}
	}
}
