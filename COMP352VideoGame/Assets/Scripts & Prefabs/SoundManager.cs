﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	
	public AudioSource FXSource;
	public List<AudioSource> listOfSongs;
	public int trackNo = 0;
	public static SoundManager instance = null;
	
	public float lowShift = .95f;
	public float highShift = 1.05f;

	// Use this for initialization
	void Start () {
		
	}
	
	void Awake () {
		if (instance == null) {
			instance = this;
			SetMusic(trackNo);
		} else if (instance != this) {
			
			// ###
			if(instance.trackNo != trackNo) {
				instance.SetMusic(trackNo);
			}
			
			Destroy(gameObject);
		}
		
		DontDestroyOnLoad(gameObject);
	}
	
	public void SetMusic (int newTrackNo) {
		listOfSongs[trackNo].Stop();
		listOfSongs[newTrackNo].Play();
		trackNo = newTrackNo;
	}
	
	public void PlaySingle (AudioClip clip) {
		FXSource.clip = clip;
		FXSource.Play();
	}
	
	public void RandomiseSFX (params AudioClip [] clips) {
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(lowShift, highShift);
		
		FXSource.clip = clips[randomIndex];
		FXSource.pitch = randomPitch;
		FXSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
